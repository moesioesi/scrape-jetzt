declare var casper : Casper;
declare var phantom : Phantom;
declare var jQuery : any;
declare var JETZT  : any;
var fs = require('fs');

phantom.cookiesEnabled = true;

casper.test.begin('gimme the messages', 5, (test : Tester) => {
    var averageTime : number;
    
    casper.options.onResourceRequested = function(context : any, requestData : any, request : any) {
        var requestLog = JSON.stringify(requestData);
        if (requestLog.indexOf('"url":"https://jetzt.sueddeutsche.de') < 0 &&
            requestLog.indexOf('"url":"http://jetzt.sueddeutsche.de') < 0) return;
        if (requestLog.indexOf("facebook.com") >= 0) return;
        if (requestLog.indexOf('"contentType":"image/') >= 0) return;
        if (requestLog.indexOf('jetzt.sueddeutsche.de/styles/') >= 0) return;
        if (requestLog.indexOf('jetzt.sueddeutsche.de/js/') >= 0) return;
        //console.log("=== REQUEST:\t\t" + `${requestData.method} : ${requestData.url}`);
    };
    casper.options.onResourceReceived = function(context : any, response : HttpResponse) {
        var responseLog =  JSON.stringify(response);
        if (responseLog.indexOf('"url":"https://jetzt.sueddeutsche.de') < 0 &&
            responseLog.indexOf('"url":"http://jetzt.sueddeutsche.de') < 0) return;
        if (responseLog.indexOf("facebook.com") >= 0) return;
        if (responseLog.indexOf('"contentType":"image/') >= 0) return;
        if (responseLog.indexOf('jetzt.sueddeutsche.de/styles/') >= 0) return;
        if (responseLog.indexOf('jetzt.sueddeutsche.de/js/') >= 0) return;
        
        //console.log("=== RESPONSE:\t\t" + `${response.status} : ${response.url}`);
    };
    
    casper.start();
    
    casper.thenOpen('http://jetzt.sueddeutsche.de/', function(response : HttpResponse) {
        casper.click('#offline a');
        test.assertExists('form[name="ajaxLogin"]', 'check for form');
        var userdata = JSON.parse(fs.read('settings.json'));
        casper.sendKeys("#login-userid", userdata.username);
        casper.sendKeys("#login-password", userdata.password);
        casper.evaluate(function login() {
            JETZT.ui.topbar.login();
        });
        
        casper.wait(2000, () => {
            casper.waitForSelector('#logout-form', null, null, 10000);
        });
    });
    
    function deleteMore() {
        var start = new Date().getTime();
        // open messages
        casper.waitForSelector('.icon.msg.tooltip', () => {
            casper.click('.icon.msg.tooltip');
            casper.waitForSelector(".usertxt", () => {
                var data = casper.evaluate(function login() {
                    return {
                        totalPages : jQuery('.page').last().attr('title').replace('Seite ', ''),
                        messageUrl : jQuery('.usertxt:first').attr('href')
                    };
                });
                
                var messagesLeft = Number(data.totalPages) * 10;
                test.assert(data.messageUrl != null, "No more messages");
                casper.open('http://jetzt.sueddeutsche.de' + data.messageUrl, null);
                var loadMessage = () => {
                    var content = casper.evaluate(function getContent() {
                        var usertxtb = jQuery('.usertxtb');
                        var username = usertxtb.text();
                        var attachments : string[] = [];
                        if (usertxtb.length > 1)
                        {
                            var users : string[] = [];
                            usertxtb.each(function() {
                                var href = jQuery(this).attr('href');
                                if (href.indexOf('/jetztpage') === 0)
                                    users.push(jQuery(this).text());
                                else if (href.indexOf('/attachment') === 0)
                                    attachments.push(href);
                            });
                            username = users.join(' - ');
                        }
                        
                        return {
                            subject : jQuery('.content .text h1').text(),
                            from : username,
                            date : jQuery(jQuery('table.botschaften td')[5]).text(),
                            body : jQuery(jQuery('table.botschaften td')[6]).text(),
                            attachments : attachments
                        };
                    });
                    
                    console.log(`[${content.date}] Storing message ${content.subject} from ${content.from}...`);
                    fs.write(`messages/${content.from}.txt`, JSON.stringify(content, undefined, 2) + ",", 'a');    
                    if (content.attachments.length > 0) {
                        content.attachments.forEach(attachment => {
                            console.log(`[${content.date}] Storing attachment from ${content.from} to ${attachment}.jpg...`);
                            casper.download('http://jetzt.sueddeutsche.de' + attachment, `messages/${content.from}/${attachment}.jpg`);
                        })
                    }
                        
                    console.log(`[${content.date}] Deleting message ${content.subject} from jetzt.de...`);
                    casper.click('.delete');
                    
                    casper.wait(500, () => {
                        var end = new Date().getTime();
                        var timeForRunInMs = end-start;
                        
                        averageTime = ((averageTime || timeForRunInMs) + timeForRunInMs) / 2;
                        var timeleft = (messagesLeft * averageTime);
                        var hours = Math.floor(timeleft / 36e5),
                            minutes = Math.floor(timeleft % 36e5 / 60000),
                            seconds = Math.floor(timeleft % 60000 / 1000);
                        console.log(`[${content.date}] Estimated time left: ${hours}:${minutes}:${seconds}`);
                        deleteMore();
                    });                    
                }
                
                casper.waitForSelector(".usertxtb", loadMessage, loadMessage, 1000);
            });
        });
    }
    
    casper.then(function() {
        casper.wait(2, deleteMore);
    });
    
    casper.run(() => {
        console.log("complete");
        test.done();
    });
});