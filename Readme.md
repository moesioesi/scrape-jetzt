# Scrape-jetzt

## Requirements

* nodejs
* npm
* tsd

## Installation

Download this repository and open a terminal (shell, cmd, powershell)

Open the folder and: 

```shell
npm install
tsd reinstall
```

## Start

Before starting, open the settings.json and set your username and password

```shell
npm start
```