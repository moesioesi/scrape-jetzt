var express = require('express');
var app = express();
import fs = require('fs');

app.set('view engine', 'jade');

interface Message {
    attachment : string;
    body : string;
    date : string;
    from : string;
    subject : string;
}

class MessagesDataProvider {
    files : string[];
    
    public constructor() {
        this.files = fs.readdirSync('./messages')
            .filter(_ => _.indexOf(".txt") > 0)
            .map(_ => _.replace(".txt", ""))
            .filter(_ => _ && _ != "");
        console.log("%d conversations loaded", this.files.length);
    }
    
    public getAllConversations() : string[] {
        return this.files;
    }
    
    public load(user : string, callback : (messages : Message[]) => void) {
        var readStream = fs.createReadStream(`./messages/${user}.txt`);
        var result = "[ ";
        readStream
            .on('readable', function () {
                var chunk = "";
                while (null !== (chunk = readStream.read())) {
                    result += chunk;
                }
            })
            .on('end', function () {
                result = result.replace(/(^,)|(,$)/g, "");
                result += "]"
                callback(JSON.parse(result));
            });
    }
}

var messages = new MessagesDataProvider();

app.get('/user/:user', function (req : any, res : any) {
    messages.load(req.params.user, (mgs) => {        
        res.render("conversation", { 
            user : req.params.user, 
            messages: mgs, 
            attachments : mgs.filter(_ => _.attachment && _.attachment.length > 0).map(_ => _.attachment)
        });
    });    
});

app.get('/', function (req : Express.Request, res : any) {
    res.render("index", { conversations: messages.getAllConversations()});
});

app.use(`/attachment`, express.static('messages'));

var server = app.listen(1773, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log('Running. Open the following link to start: http://localhost:%s', port);
});